#!/usr/bin/env python3
from thesaurus import Word
import sys
import asyncio
import aiohttp

async def get_word(word):
        tasks = []
        async with aiohttp.ClientSession() as session:
                wordobj = Word(word)
                tasks.append(wordobj.fetchWordData(session))
                await asyncio.gather(*tasks)
        return wordobj


ow = sys.argv[1]
w = asyncio.run(get_word(ow))

print('("' + ow + '"')
print(' (;synonyms')
for x in w.data:
	print('  ("' + x['meaning'] + '" "' + x['partOfSpeech'] + '"')
	for s in x['syn']:
		print('   ("' + s[0] + '" . ' + str(s[1]) + ')')
	print('  )')

print(' )')

print(' (;antonyms')
for x in w.data:
	print('  ("' + x['meaning'] + '" "' + x['partOfSpeech'] + '"')
	for s in x['ant']:
		print('   ("' + s[0] + '" . ' + str(s[1]) + ')')
	print('  )')

print(' )')
print(')')
