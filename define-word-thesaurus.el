;;; define-word-thesaurus.el --- display Thesaurus.com synonyms of word at point. -*- lexical-binding: t -*-

;; Copyright (C) 2017 Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; URL: https://gitlab.com/andersjohansson/define-word-thesaurus
;; Version: 0.2.0
;; Modified: 2024-08-04
;; Package-Requires: ((define-word "0.1") (emacs "25.1"))
;; Keywords: dictionary, convenience

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; Adds thesaurus.com to define-word

;;; Code:
(require 'define-word)
(require 'cl-lib)
(require 'subr-x)

;;;; Defining the service
(add-to-list 'define-word-services
             '(thesaurus define-word-thesaurus-get-synonyms ignore) t)
(add-to-list 'define-word-displayfn-alist (cons 'thesaurus #'define-word-thesaurus-display) t)

;;;; Custom variables
(defgroup define-word-thesaurus nil
  "Customization of define-word-thesaurus"
  :group 'define-word)

(defface define-word-thesaurus-rel-3 '((t (:inherit bold)))
  "Face for most relevant words")
(defface define-word-thesaurus-rel-2 '((t (:inherit default)))
  "Face for relevant words")
(defface define-word-thesaurus-rel-1 '((t (:inherit italic)))
  "Face for least relevant words")
(defface define-word-thesaurus-header '((t (:height 1.4 :inherit bold)))
  "Face for buffer header")
(defface define-word-thesaurus-subheader '((t (:height 1.2 :inherit (bold italic))))
  "Face for buffer header")

;;;; Variables
(defvar define-word-thesaurus-history nil)
(defvar define-word-thesaurus-history-forward nil)

(defvar define-word-thesaurus-command-name
  (expand-file-name "thesaurus-to-emacs.py"
                    (or (and load-file-name
                             (file-name-directory load-file-name))
                        default-directory))
  "Command name for invoking the python script “thesaurus-to-emacs.py”.
Should be automatically initialized to point to the script in the
directory of “define-word-thesaurus.el”")

(defvar-local define-word-thesaurus-current-data nil
  "Current thesaurus word lists")
(defvar define-word-thesaurus-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-o") #'define-word-thesaurus-follow-link-at-point)
    (define-key map (kbd "<RET>") #'define-word-thesaurus-follow-link-at-point)
    (define-key map [mouse-2] #'define-word-thesaurus-click-link)
    (define-key map [follow-link] 'mouse-face)
    (define-key map (kbd "C-c C-c") #'define-word-thesaurus-goto-online-link-at-point)
    ;; TODO, how should this be bound?
    ;; (define-key map (kbd "C-S-<mouse-1>") #'define-word-thesaurus-click-link)
    (define-key map (kbd "g") nil)
    (define-key map (kbd "l") #'define-word-thesaurus-history-back)
    (define-key map (kbd "C-c C-b") #'define-word-thesaurus-history-back)
    (define-key map (kbd "<XF86Back>") #'define-word-thesaurus-history-back)
    (define-key map (kbd "r") #'define-word-thesaurus-history-forward)
    (define-key map (kbd "C-c C-f") #'define-word-thesaurus-history-forward)
    (define-key map (kbd "<XF86Forward>") #'define-word-thesaurus-history-forward)
    (define-key map (kbd "f") #'define-word-thesaurus-forward-term)
    (define-key map (kbd "<tab>") #'define-word-thesaurus-forward-term)
    (define-key map (kbd "b") #'define-word-thesaurus-backward-term)
    (define-key map (kbd "<backtab>") #'define-word-thesaurus-backward-term)
    (define-key map (kbd "n") #'define-word-thesaurus-forward-line)
    (define-key map (kbd "C-M-n") #'define-word-thesaurus-next-sense)
    (define-key map (kbd "C-M-p") #'define-word-thesaurus-previous-sense)
    (define-key map (kbd "s") #'define-word-thesaurus-search)
    ;; (define-key map (kbd "p") #'backward-line)
    map)
  "Keymap for `define-word-thesaurus-mode'.")

;;;; Search command
;;;###autoload
(defun define-word-thesaurus-search (&optional word)
  "Search for WORD (prompted for in interactive call) in thesaurus.com"
  (interactive "Mword: ")
  (define-word word 'thesaurus))

;;;; Functions
(defmacro define-word-thesaurus--with-thesaurus-buffer (&rest body)
  "Execute body in thesaurus buffer, that is created or switched
to and displayed."
  (declare (indent 0))
  `(let ((buf (get-buffer-create "*Thesaurus*"))
         (inhibit-read-only t))
     (unless (eq buf (current-buffer))
       (display-buffer buf))
     (with-current-buffer buf
       ,@body)))

(defun define-word-thesaurus-get-synonyms (word)
  (let ((buf (generate-new-buffer "*thesaurus-parse*")))
    (if (eq 0 (call-process define-word-thesaurus-command-name nil (list buf nil) nil word))
        (with-current-buffer buf
          (goto-char (point-min))
          (prog1
              (let ((data (read buf))
                    ;; we get an early creation and display of
                    ;; the buffer. In normal cases, that’s ok
                    (window-width (define-word-thesaurus--with-thesaurus-buffer (window-width))))
                (concat
                 (propertize (pop data) 'face 'define-word-thesaurus-header) "\n"
                 (propertize "Synonyms" 'face 'define-word-thesaurus-subheader) "\n"
                 (define-word-thesaurus--compile-word-list (pop data) window-width)
                 (propertize "Antonyms" 'face 'define-word-thesaurus-subheader) "\n"
                 (define-word-thesaurus--compile-word-list (pop data) window-width)))
            (kill-buffer buf)))
      (user-error "Failed retrieving synonyms. Non-existing word?"))))

(defun define-word-thesaurus--compile-word-list (data window-width)
  (cl-loop
   for sense in data concat
   (when (< 2 (length sense))
     (concat
      "‣ " (propertize (cadr sense) 'face 'italic) " "
      (car sense)
      (propertize "\n" 'section 'sense)
      (define-word-thesaurus--strings-in-columns
        (cl-loop
         for syn in
         (cl-sort (cddr sense) #'> :key #'cdr)
         collect
         (propertize
          (car syn)
          'mouse-face 'highlight
          'help-echo "mouse-2: Search this word"
          'face (cl-case (cdr syn)
                  (3 'define-word-thesaurus-rel-3)
                  (2 'define-word-thesaurus-rel-2)
                  (1 'define-word-thesaurus-rel-1)
                  (_ 'default))))
        window-width)
      "\n"))))

(defun define-word-thesaurus-display (disp &optional historywalk)
  "Display a results buffer for thesaurus.com search
DISP is the string to be displayed. HISTORYWALK indicates that we
are navigating the `define-word-thesaurus-history'."
  (define-word-thesaurus--with-thesaurus-buffer
    (delete-region (point-min) (point-max))
    (when (and define-word-thesaurus-current-data (not historywalk))
      (push define-word-thesaurus-current-data define-word-thesaurus-history))
    (insert disp)
    (define-word-thesaurus-mode)
    (goto-char (point-min))
    (setq define-word-thesaurus-current-data disp)))


;;;; Mode definition and navigation functions
(define-derived-mode define-word-thesaurus-mode special-mode "Thesaurus mode"
  "Mode for displaying thesaurus.com results")

(defun define-word-thesaurus-click-link (event)
  "Follows clicked link in `define-word-thesaurus-mode'"
  (interactive "e")
  (let ((window (posn-window (event-end event)))
        (pos (posn-point (event-end event))))
    (if (not (windowp window))
        (error "No selection chosen"))
    (with-current-buffer (window-buffer window)
      (goto-char pos)
      (define-word-thesaurus-follow-link-at-point))))

(defun define-word-thesaurus-follow-link-at-point ()
  "Follow link at point in `define-word-thesaurus-mode'"
  (interactive)
  (let ((word (define-word-thesaurus--get-word-at-point)))
    (unless (string-blank-p word)
      (define-word word 'thesaurus))))

(defun define-word-thesaurus-history-back ()
  "Navigates back in history of thesaurus.com searches."
  (interactive)
  (when define-word-thesaurus-history
    (push define-word-thesaurus-current-data
          define-word-thesaurus-history-forward)
    (define-word-thesaurus-display (pop define-word-thesaurus-history) t)))

(defun define-word-thesaurus-history-forward ()
  "Navigates forward in history of thesaurus.com searches."
  (interactive)
  (when define-word-thesaurus-history-forward
    (push define-word-thesaurus-current-data
          define-word-thesaurus-history)
    (define-word-thesaurus-display (pop define-word-thesaurus-history-forward) t)))

(defun define-word-thesaurus-goto-online-click (event)
  "Follows clicked link in `define-word-thesaurus-mode'"
  (interactive "e")
  (let ((window (posn-window (event-end event)))
        (pos (posn-point (event-end event))))
    (if (not (windowp window))
        (error "No selection chosen"))
    (with-current-buffer (window-buffer window)
      (goto-char pos)
      (define-word-thesaurus-goto-online-link-at-point))))

(defun define-word-thesaurus-goto-online-link-at-point ()
  "Goto online definition of link at point in `define-word-thesaurus-mode'"
  (interactive)
  (let ((word (define-word-thesaurus--get-word-at-point)))
    (unless (string-blank-p word)
      (browse-url (format "http://www.thesaurus.com/browse/%s" word)))))

(defun define-word-thesaurus--get-word-at-point ()
  (let ((pos (point)))
    (string-trim
     (buffer-substring-no-properties
      (previous-single-property-change pos 's-col nil (point-at-bol))
      (next-single-property-change pos 's-col nil (point-at-eol))))))

;;;; Movement functions
(defun define-word-thesaurus-forward-term ()
  (interactive)
  (when-let ((next (next-single-property-change (point) 's-col))
             (move (if (get-text-property (point) 's-col)
                       (next-single-property-change next 's-col)
                     next)))
    (goto-char move)))

(defun define-word-thesaurus-backward-term ()
  (interactive)
  (when-let ((prev (previous-single-property-change (point) 's-col))
             (move (if (get-text-property (point) 's-col)
                       (previous-single-property-change prev 's-col)
                     prev)))
    (goto-char move)))

;; TODO, still not very good
(defun define-word-thesaurus-forward-line ()
  (interactive)
  (if-let ((col (get-text-property (point) 's-col)))
      (cl-loop initially do (forward-line)
               until
               (if-let
                   ((matchcol
                     (text-property-any (point-at-bol) (point-at-eol)
                                        's-col col)))
                   (goto-char matchcol)
                 (cl-loop
                  with found = -1
                  with limit = (- (point-at-eol) 1)
                  until (eq found limit)
                  do (goto-char
                      (setq found
                            (next-single-property-change
                             (point) 's-col nil limit)))
                  finally return t)))
    (define-word-thesaurus-forward-term)))

(defun define-word-thesaurus-next-sense ()
  (interactive)
  (when-let (next (next-single-property-change (point) 'section))
    (goto-char (1+ next))))

(defun define-word-thesaurus-previous-sense ()
  (interactive)
  (when-let (prev (previous-single-property-change (1- (point)) 'section))
    (goto-char prev)))


;;;; Format columns
(defun define-word-thesaurus--strings-in-columns (strings &optional window-width)
  "Return list of STRINGS in columns
Fills columns to WINDOW-WIDTH, or ‘window-width’."
  ;; adapted from `ls-lisp-column-format' from:
  ;; https://www.emacswiki.org/emacs/ls-lisp-20.el
  (with-temp-buffer
    (let* ((colwid (+ 1 (cl-loop for x in strings
                                 maximize (length x))))
           (nstrings (length strings))
           (fmt (format "%%-%ds" colwid))	; print format
           (ncols (/ (or window-width (window-width)) colwid)) ; no of columns
           (collen (/ nstrings ncols)))
      (if (> nstrings (* collen ncols)) (setq collen (1+ collen)))
      (let ((i 0) j c)
        (while (< i collen)
          (setq j i
                c 0)
          (while (< j nstrings)
            (insert (format fmt
                            (concat (propertize (nth j strings) 's-col c) " ")
                            ))
            (setq j (+ j collen))
            (cl-incf c))
          (insert ?\n)
          (setq i (1+ i)))))
    (whitespace-cleanup)
    (buffer-string)))

(provide 'define-word-thesaurus)

;;; define-word-thesaurus.el ends here

